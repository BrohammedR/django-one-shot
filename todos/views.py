from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm, TodoItemForm
# Create your views here.

def todo_list(request):
    list = TodoList.objects.all()
    context={
        "list": list,
    }
    return render(request, 'todos/todos_list.html', context)

def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    items = todo_list.items.all()
    context={
        'todo_list':todo_list,
        'items':items,
    }
    return render(request, 'todos/todo_list_detail.html', context)

def todo_list_create(request):
    if request.method == "POST":
        form= TodoListForm(request.POST)
        if form.is_valid():
            todolist= form.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form=TodoListForm()
    context={
        "form":form
    }
    return render(request, 'todos/todo_list_create.html', context)

def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method=="POST":
        form=TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form=TodoListForm(instance=todo_list)
    context={
        'todo_list_object':todo_list,
        "form":form
    }
    return render(request, "todos/edit.html", context)



def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == 'POST':
        todo_list.delete()
        return redirect('todo_list_list')
    return render(request, "todos/todo_list_delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form= TodoItemForm(request.POST)
        if form.is_valid():
            itemlist=form.save()
            return redirect("todo_list_detail", id=itemlist.list.id)
    else:
        form=TodoItemForm()
    context={
        "form":form
    }
    return render(request, 'todos/items/create.html', context)
